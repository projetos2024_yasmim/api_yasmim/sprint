const router = require('express').Router()
const usuarioController = require('../controller/usuarioController.js')




router.get('/getUser', usuarioController.getUsuario)
router.get('/Login', usuarioController.Login)
router.put('/usuarioAtualizar', usuarioController.updateUsuario)
router.delete('/deleteUser/:id', usuarioController.deleteUsuarioById);
router.post('/usuarioPost', usuarioController.postUsuario);




module.exports = router;