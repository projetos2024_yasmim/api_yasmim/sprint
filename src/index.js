const express = require('express'); 
const cors = require('cors'); 
const apiRoutes = require('./routes/apiRoutes.js');

class AppController {
    constructor() {
      this.express = express(); 
      this.middlewares(); 
      this.routes(); 
    }

    middlewares() {
      this.express.use(express.json()); 
      this.express.use(cors()); 
    }

    routes() {
      // Usa as rotas definidas no arquivo apiRoutes.js
      this.express.use('/api/', apiRoutes); 
      
      // Rota de teste
      this.express.get("/teste/", (_, res) => {  // Define uma rota de teste
        res.send({ status: "Api Ligada." }); // Retorna uma mensagem indicando que a API está ligada
      })
    }
}

module.exports = new AppController().express; // Exporta a instância do express configurada com os middlewares e rotas
