module.exports = class usuarioController {  
    static async postUsuario(req, res) { 
        const { user, senha } = req.body; //Extrai os campos user e senha da solicitação HTTP
        
        if (user === "" || senha === "") { 
            return res.status(400).json({ message: "Por favor, preencha os campos corretamente" }); 
        } else { 
            
            return res.status(200).json({ message: "Usuário criado com sucesso." }); 
        }
    }
    
    

    static async updateUsuario(req, res) { 
        
            const { user, senha } = req.body; 
            if(user ===""||senha ===""){ 
                return res.status(200).json({ message: "Preencha os campos corretamente" });  
            }

            else{
                return res.status(200).json({ message: "Usuário atualizado" });  
            } 
    }

    static async getUsuario(req, res) {
        const user = "yasmim";
        const senha = "yasmim123";
        return res.status(200).json({ user: user, senha: senha });
    }
    

    static async deleteUsuarioById(req, res) {
        const { id } = req.params;
    
        // Verificar se o ID é válido (por exemplo, se é um número inteiro positivo)
        if (!id || isNaN(id) || parseInt(id) <= 0) {
            return res.status(400).json({ message: "ID de usuário inválido!" });
        }
    
        // Aqui você irá implementar a lógica para excluir o usuário com o ID fornecido
        // Por enquanto, vamos apenas retornar uma mensagem indicando que o usuário foi removido
        return res.status(200).json({ message: "Usuário removido com ID " + id });
    }
    
    static async Login(req, res) { 
        const { user, senha } = req.body; // Extrai os campos user e senha da query da solicitação HTTP
        
        // Log para verificar os valores de user e senha recebidos na solicitação
        console.log("Usuário:", user);
        console.log("Senha:", senha);
        
        // Verifica se o usuário e a senha estão corretos
        if (user === "yasmim" && senha === "yasmim123") { 
            // Log para indicar que as credenciais são válidas
            console.log("Credenciais válidas. Login bem-sucedido.");
            return res.status(200).json({ message: "Login bem-sucedido." }); 
        } else { 
            // Log para indicar que as credenciais são inválidas
            console.log("Credenciais inválidas.");
            return res.status(401).json({ message: "Credenciais inválidas." }); 
        }
    }
    
    
    
    
}
  



    
    
